terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }

  required_version = ">= 0.14.9"
}

provider "aws" {
  profile = "default"
  region  = "eu-south-1"
}

resource "aws_codecommit_repository" "devops-infra-modules" {
  repository_name = "devops-infra-modules"
  description     = "Terraform infra modules repository"
  default_branch = "master"
  tags = {
    Terraform = "True"
  }
}

resource "aws_codecommit_repository" "devops-infra" {
  repository_name = "devops-infra"
  description     = "Terrgrunt infra configuration repository"
  default_branch = "master"
  tags = {
    Terraform = "True"
  }
}