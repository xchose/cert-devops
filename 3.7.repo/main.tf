terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }

  required_version = ">= 0.14.9"
}

provider "aws" {
  profile = "default"
  region  = "eu-south-1"
}

//Repository my-webpage
resource "aws_codecommit_repository" "my-webpage" {
  repository_name = "my-webpage"
  description     = "my-webpage repository for cert learning lab"
  default_branch = "master"
  tags = {
    Terraform = "True"
  }
}

//IAM Grup
resource "aws_iam_group" "git-group" {
  name = "git-group"
}

//IAM user
resource "aws_iam_user" "git-user" {
  name = "git-user"
  tags = {
    Terraform = "True"
  }
}

//Attach user to group
resource "aws_iam_user_group_membership" "git" {
  user = aws_iam_user.git-user.name
  groups = [
    aws_iam_group.git-group.name,
  ]   
}

//Policy
resource "aws_iam_group_policy_attachment" "test-attach" {
  group       = aws_iam_group.git-group.name
  policy_arn = "arn:aws:iam::aws:policy/AWSCodeCommitFullAccess"
}


